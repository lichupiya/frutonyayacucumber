package actions;

import locators.LoginPageLocators;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.HelperClass;

import static java.lang.Thread.sleep;



public class LoginFormActions {

    LoginPageLocators loginPageLocators;

    public LoginFormActions() {
        this.loginPageLocators = new LoginPageLocators();
        PageFactory.initElements(HelperClass.getDriver(), loginPageLocators);
    }

    public LoginFormActions clickButtonLogin(String name) {
        loginPageLocators.buttonLogin.click();
        return this;
    }

    public void setInputEmail(String name) {
        loginPageLocators.inputEmail.sendKeys(name);
    }

    public void setInputPassword(String name) {
        loginPageLocators.temporaryPassword.sendKeys(name);
    }

    public String checkTextEmail(String name) {
        return loginPageLocators.inputEmail.getAttribute("value");
    }

}

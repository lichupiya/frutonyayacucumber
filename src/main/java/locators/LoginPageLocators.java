package locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageLocators {

    @FindBy(xpath = "//*[@id=\"USER_LOGIN\"]")
    public WebElement inputEmail;

    @FindBy(xpath = "//*[@name=\"USER_PASSWORD\"]")
    public WebElement temporaryPassword;

    @FindBy(css = "[class*=\"formPage__submit_recover\"]")
    public WebElement buttonLogin;

}

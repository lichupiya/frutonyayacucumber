# language: ru

@smoke
Функция: : Сайт фрутоняня

  @regression @case1
 Структура сценария: Негативная авторизация ЛК
    Дано Пользователь открывает сайт "https://frutonyanya.ru/auth/"
    Когда Пользователь вводит e-mail "<incorrectLogin>"
    Когда Пользователь вводит пароль "<incorrectPassword>"
    Тогда Проверить заполнение поля e-mail "<expectedIncorrectLogin>"
    Тогда Пользователь нажимает кнопку "Войти"
    Примеры:
      | incorrectLogin | incorrectPassword | expectedIncorrectLogin |
      | test@test.test | test              | test@test.test         |


  @regression @case2
  Структура сценария: Положительная авторизация ЛК
    Дано Пользователь открывает сайт "https://frutonyanya.ru/auth/"
    Когда Пользователь вводит e-mail "<login>"
    Когда Пользователь вводит пароль "<password>"
    Тогда Проверить заполнение поля e-mail "<expectedLogin>"
    Тогда Пользователь нажимает кнопку "Войти"
    Примеры:
      | login                  | password   | expectedLogin      |
      | cpk54348@fosiq.com | qwerty$444 | cpk54348@fosiq.com |

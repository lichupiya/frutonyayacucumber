package steps;
import actions.LoginFormActions;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import locators.LoginPageLocators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import utils.HelperClass;

import java.time.Duration;

public class SetpDefinitions {

    LoginFormActions objLoginPage = new LoginFormActions();

    @BeforeAll
    public static void startUp() {
        HelperClass.setUpDriver();
    }

    @AfterAll
    @Order(1)
    static void destroy() {
        HelperClass.tearDown();
    }

    @Дано("Пользователь открывает сайт {string}")
    public void openSite(String url) {
        HelperClass.openPage(url);
    }


    @Тогда("Пользователь нажимает кнопку {string}")
    public void пользовательНажимает(String name) {
        objLoginPage.clickButtonLogin(name);
    }


    @Когда("Пользователь вводит e-mail {string}")
    public void пользовательВводитEmail(String arg0) {

        objLoginPage.setInputEmail((arg0));
    }


    @Тогда("Проверить заполнение поля e-mail {string}")
    public void проверитьЗаполнениеПоляEmail(String email) {
        String result = objLoginPage.checkTextEmail(email);
        Assertions.assertEquals(email, result);
    }

    @Когда("Пользователь вводит пароль {string}")
    public void inputPassword(String password) {
        objLoginPage.setInputPassword(password);

    }

}
